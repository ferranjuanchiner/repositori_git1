/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam.m03.uf4.garatge;

/**
 *
 * @author Fjuan
 */
public class Automobil extends Vehicle {
    int numPlaces;

    public Automobil(double potencia, String matricula, int numPlaces) {
        super(potencia, matricula);
        super.potencia = potencia;
        super.matricula = matricula;
        this.numPlaces = numPlaces;
    }

    public double quota() {
        return (numPlaces * potencia) / 2;

    }

    public String toString() {
        return "Automòbil/"+matricula+"/"+potencia+" CV";

    }
}
