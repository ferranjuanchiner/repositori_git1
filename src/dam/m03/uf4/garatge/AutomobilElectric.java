/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam.m03.uf4.garatge;

/**
 *
 * @author Fjuan
 */
public class AutomobilElectric extends Automobil implements Electric {
    public AutomobilElectric(double potencia, String matricula, int
numPlaces){
        super(potencia, matricula,numPlaces);
    }
    
    public double quota(){
        return (super.quota()+consumMensual());
    }
    public String toString(){
       return "Automòbil Elèctric/" + matricula + "/" + potencia + " CV";
        
    }
    public double consumMensual(){
        return  QUOTAELECTRICABASE;
        
    }
}
