/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam.m03.uf4.garatge;

/**
 *
 * @author Fjuan
 */
public class Garatge {

    static int numPlaces;
    static int nplaca = 0;
    static Vehicle[] vehicles;

    public Garatge(int numPlaces) {
        Garatge.numPlaces = numPlaces;
        vehicles = new Vehicle[numPlaces];
    }

    public int altaVehicle(Vehicle vehicle) {
        if (nplaca >= numPlaces) {
            System.out.println("No hi han places disponibles per al vehicle " + vehicle);
            return -1;
        } else {
            vehicles[nplaca] = vehicle;
            System.out.println("Assignada plaça número " + (nplaca + 1) + " al vehicle " + vehicle);
            nplaca++;
            return nplaca;
        }

    }

    public boolean baixaVehicle(int numPlaça) {
        return false;
    }

    public void mostrarVehicles() {
        if (nplaca == 0) {
            System.out.println("No hi ha vehicles per llistar al garatge");
        } else {
            System.out.println("Llistat de vehicles al Garatge: ");
            for (int n = 0; n < nplaca; n++) {
                System.out.println("(" + (n + 1) + "): " + vehicles[n]);

            }
        }

    }
}
