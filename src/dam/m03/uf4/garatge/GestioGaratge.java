/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam.m03.uf4.garatge;

/**
 *
 * @author jmartin
 */
public class GestioGaratge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Garatge garatge = new Garatge(4); // Creem un garatge amb 4 només places
        Automobil a1 = new Automobil(100, "1234ABC", 5);
        Automobil a2 = new AutomobilElectric(150, "4321CBA", 2);
        Motocicleta m1 = new Motocicleta(30, "5678ABC");
        Motocicleta m2 = new MotocicletaElectrica(50, "8765CBA");
        Motocicleta m3 = new MotocicletaElectrica(40, "0000AAA");
        garatge.altaVehicle(a1);
        garatge.altaVehicle(a2);
        garatge.altaVehicle(m1);
        garatge.altaVehicle(m2); 
        garatge.altaVehicle(m3); // No hauria de deixar
        
        System.out.println("Quota " + a1 +": " + a1.quota() + " €");
        System.out.println("Quota " + a2 +": " + a2.quota() + " €");
        System.out.println("Quota " + m1 +": " + m1.quota() + " €");
        System.out.println("Quota " + m2 +": " + m2.quota() + " €");
        
        garatge.baixaVehicle(2);
        garatge.mostrarVehicles();
    }
    
}
