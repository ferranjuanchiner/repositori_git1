/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam.m03.uf4.garatge;

/**
 *
 * @author Fjuan
 */
public class Motocicleta extends Vehicle {

    public Motocicleta(double potencia, String matricula) {
        super(potencia, matricula);
        super.potencia = potencia;
        super.matricula = matricula;

    }

    public double quota() {
        return (potencia * 2);

    }

    public String toString() {
        return "Motocicleta/" + matricula + "/" + potencia + " CV";

    }
}
