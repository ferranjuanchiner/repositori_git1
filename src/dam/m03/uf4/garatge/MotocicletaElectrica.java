/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam.m03.uf4.garatge;

/**
 *
 * @author Fjuan
 */
public class MotocicletaElectrica extends Motocicleta implements Electric {

    public MotocicletaElectrica(double potencia, String matricula) {
        super(potencia, matricula);
    }

    public double quota() {
        return (super.quota() + consumMensual());

    }

    public String toString() {
        return "Motocicleta Elèctrica/" + matricula + "/" + potencia + " CV";

    }

    public double consumMensual() {
        return (QUOTAELECTRICABASE / 2);

    }
}
